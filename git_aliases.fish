#!/usr/bin/fish

function gs --wraps='git status' --description 'git status'
  git status $argv;
end

function gl --wraps='git log --pretty=tformat:"%C(yellow)%h %Cred%ad %C(white)%an%C(auto)%d %C(green)%s" --date=short --graph --all' --description 'красивый гит лог ВСЕХ веток. (auto) НЕ РАБОТАЕТ на git 1.X (старее 2 версии), заменить на цвет, например, (red)'
  git log --pretty=tformat:"%C(yellow)%h %Cred%ad %C(white)%an%C(auto)%d %C(green)%s" --date=short --graph $argv;
end

function glb --wraps='git log --pretty=tformat:"%C(yellow)%h %Cred%ad %C(white)%an%C(auto)%d %C(green)%s" --date=short --graph' --description 'красивый гит лог ТЕКУЩЕЙ ветки. (auto) НЕ РАБОТАЕТ на git 1.X (старее 2 версии), заменить на цвет, например, (red)'
  git log --pretty=tformat:"%C(yellow)%h %Cred%ad %C(white)%an%C(auto)%d %C(green)%s" --date=short --graph $argv;
end

function gco --wraps='git checkout' --description 'сменить ветку, чекаутнуться на коммит (для просмотра). gco -b branch создаст ветку branch и перейдет на нее'
  git checkout $argv;
end

function gc --wraps='git commit -m' --description 'закоммитить все что в индексе. После этой команды пишем в кавычках текст коммита'
  git commit -m $argv;
end

function gca --wraps='git commit -m' --description 'закоммитить все измененное, кроме untracked. После этой команды пишем в кавычках текст коммита'
  git commit -a -m $argv;
end

function gd --wraps='git diff -w' --description 'ПОЛЕЗНАЯ команда. дифф только измененного кода. не покажет изменения прав файлов, пустых строк, отступов'
  git diff -w $argv;
end

function gm --wraps='git merge -Xignore-all-space --no-edit' --description 'мердж, который не создаст конфликтов из-за отступов, пустых строк и тд. НЕ РАБОТАЕТ на git 1.X'
  git merge -Xignore-all-space --no-edit $argv;
end

function gr --wraps='git rebase' --description 'ребейз'
  git rebase $argv;
end

function grh --wraps='git reset --hard' --description 'стереть все изменения в WD и индексе. не трогает untracked'
  git reset --hard $argv;
end

function gf --wraps='git fetch' --description 'git fetch'
  git fetch $argv;
end

function grh1 --wraps='git reset --hard HEAD~1' --description 'стереть все изменения в WD и индексе + стереть текущий коммит. удобно, когда надо просто стереть последний коммит'
  git reset --hard HEAD~1 $argv;
end

function gss --wraps='git show --stat --pretty=fuller' --description 'удобно посмотреть что в коммите. если не передавать ей хеш коммита, покажет что есть в текущем коммите'
  git show --stat --pretty=fuller $argv;
end

function gh1 --wraps='git checkout HEAD~1' --description 'глянуть предыдущий коммит'
  git checkout HEAD~1 $argv;
end

function ga --wraps='git add -v' --description 'подробный git add (с выводом отчета)'
  git add -v $argv;
end

function gt --wraps='git tag --sort version:refname' --description 'Корректный вывод тегов'
  git tag --sort version:refname $argv;
end

function gcp --wraps='git cherry-pick' --description 'git cherry-pick'
  git cherry-pick $argv;
end

function back --wraps='git reset --hard HEAD@{1}' --description 'git reset --hard HEAD@{1}'
  git reset --hard HEAD@{1} $argv;
end

function gbc --wraps='git branch -a --contains' --description 'git branch -a --contains'
  git branch -a --contains $argv;
end