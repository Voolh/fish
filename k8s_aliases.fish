#!/usr/bin/fish

function k --wraps=kubectl --description 'alias k=kubectl'
  kubectl $argv; 
end

function kd --wraps='kubectl describe' --description 'alias kd=kubectl describe'
  kubectl describe $argv; 
end

function kg --wraps='kubectl get' --description 'alias kg=kubectl get'
  kubectl get $argv; 
end

function kgcj --wraps='kubectl get cronjob' --description 'alias kgcj=kubectl get cronjob'
  kubectl get cronjob $argv; 
end

function kgj --wraps='kubectl get job' --description 'alias kgj=kubectl get job'
  kubectl get job $argv; 
end

function kgn --wraps='kubectl get nodes' --description 'alias kgn=kubectl get nodes'
  kubectl get nodes $argv; 
end

function kgp --wraps='kubectl get pod' --description 'alias kgp=kubectl get pod'
  kubectl get pod $argv; 
end

function kgpa --wraps='kubectl get pods -A' --description 'alias kgpa=kubectl get pods -A'
  kubectl get pods -A $argv; 
end

function kgpv --wraps='kubectl get pv' --description 'alias kgpv=kubectl get pv'
  kubectl get pv $argv; 
end

function kgpvc --wraps='kubectl get pvc' --description 'alias kgpvc=kubectl get pvc'
  kubectl get pvc $argv; 
end

function kgr --wraps='kubectl get role' --description 'alias kgr=kubectl get role'
  kubectl get role $argv; 
end

function kgsa --wraps='kubectl get svc -A' --description 'alias kgs=kubectl get svc -A'
  kubectl get svc -A $argv; 
end

function kl --wraps='kubectl logs' --description 'alias kl=kubectl logs'
  kubectl logs $argv; 
end