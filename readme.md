# Fish Shel Aliases

## Install fish shell
To install fish, run the following commands:

Short version:

```shell
curl -o ~/.setup-fish.sh https://gitlab.com/Voolh/fish/-/raw/master/.setup-fish.sh && \
chmod +x ~/.setup-fish.sh && \
~/.setup-fish.sh && \
rm ~/.setup-fish.sh
```

## Install fisher plugin && jethrokuan/z plugin
```shell
fish;

fish_add_path ~/.local/bin;

curl -sL https://git.io/fisher | source;

fisher install jorgebucaran/fisher;

fisher install jethrokuan/z;

fisher install edc/bass
```

You can toggle the prompt on or off like this:

```fish
kube_ps on
kube_ps off
```
