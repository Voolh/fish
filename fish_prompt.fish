#!/usr/bin/fish

function fish_prompt
  echo -s -n (set_color yellow)(whoami)(set_color white)@(set_color blue)(prompt_hostname) " " (set_color green) (fish_git_prompt) (set_color blue) (set_color $fish_color_cwd) " " (prompt_pwd) " "
end

function fish_right_prompt
    echo -s -n (__kube_prompt)
end
