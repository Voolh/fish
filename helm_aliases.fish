#!/usr/bin/fish

function h --wraps='helm' --description 'alias h=helm'
  helm $argv; 
end

function hs --wraps='helm secrets' --description 'alias hs=helm secrets'
  helm secrets $argv;
end

function hl --wraps='helm list' --description 'alias hl=helm list'
  helm list $argv;
end

function hla --wraps='helm list -A' --description 'alias hla=helm list -A'
  helm list -A $argv;
end
