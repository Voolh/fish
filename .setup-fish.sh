#!/bin/bash

if [ ! -f ~/.bash_profile ]; then
    touch ~/.bash_profile
fi

if [ -d "$HOME/.local/bin" ] && [[ ":$PATH:" != *":$HOME/.local/bin:"* ]]; then
    mkdir -p ~/.local/bin \
    && echo 'export PATH="$HOME/.local/bin:$PATH"' >> ~/.bash_profile \
    && source ~/.bash_profile
fi

sudo apt update \
&& sudo apt install -y software-properties-common git \
&& sudo apt-add-repository -y ppa:fish-shell/release-3 \
&& sudo apt update -y \
&& sudo apt install -y fish python-is-python3 \
&& curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" \
&& chmod +x kubectl \
&& mkdir -p ~/.local/bin \
&& mv ./kubectl ~/.local/bin/kubectl \
&& echo $"export PATH=\$PATH:~/.local/bin" >> ~/.bashrc \
&& source ~/.bashrc \
&& echo /usr/bin/fish | sudo tee -a /etc/shells \
&& sudo usermod -s /usr/bin/fish $USER \
&& mkdir -p ~/.config/fish/{functions,fish-kube-prompt} \
&& rm -rf ~/.config/fish/fish-kube-prompt \
&& git clone https://github.com/aluxian/fish-kube-prompt ~/.config/fish/fish-kube-prompt \
&& ln -fs ~/.config/fish/fish-kube-prompt/functions/__kube_prompt.fish ~/.config/fish/functions/ \
&& ln -fs ~/.config/fish/fish-kube-prompt/functions/kube_ps.fish ~/.config/fish/functions/ \
&& curl https://gitlab.com/Voolh/fish/-/raw/master/config.fish --output ~/.config/fish/config.fish \
&& curl https://gitlab.com/Voolh/fish/-/raw/master/docker_aliases.fish --output ~/.config/fish/functions/docker_aliases.fish \
&& curl https://gitlab.com/Voolh/fish/-/raw/master/git_aliases.fish --output ~/.config/fish/functions/git_aliases.fish \
&& curl https://gitlab.com/Voolh/fish/-/raw/master/k8s_aliases.fish --output ~/.config/fish/functions/k8s_aliases.fish \
&& curl https://gitlab.com/Voolh/fish/-/raw/master/helm_aliases.fish --output ~/.config/fish/functions/helm_aliases.fish \
&& curl https://gitlab.com/Voolh/fish/-/raw/master/minikube_aliases.fish --output ~/.config/fish/functions/minikube_aliases.fish \
&& curl https://gitlab.com/Voolh/fish/-/raw/master/fish_prompt.fish --output ~/.config/fish/functions/fish_prompt.fish \
&& curl https://gitlab.com/Voolh/fish/-/raw/master/kube_ps.fish --output ~/.config/fish/functions/kube_ps.fish
