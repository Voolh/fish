#!/usr/bin/fish

function dc --wraps='docker-compose' --description 'docker-compose'
  docker-compose $argv;
end

function dce --wraps='docker-compose exec' --description 'docker-compose exec'
  docker-compose exec $argv;
end

function di --wraps='docker images' --description 'docker images'
  docker images $argv;
end

function dcl --wraps='docker-compose logs' --description 'docker-compose logs'
  docker-compose logs $argv;
end

function dl --wraps='docker logs' --description 'docker logs'
  docker logs $argv;
end

function dsa --wraps='docker stop (docker ps -a -q)' --description 'docker stop $(docker ps -a -q)'
  docker stop (docker ps -a -q) $argv;
end

function dcps --wraps='docker-compose ps' --description 'docker-compose ps'
  docker-compose ps $argv;
end

function dps --wraps='docker ps' --description 'docker ps'
  docker ps $argv;
end

function ds --wraps='docker stop' --description 'docker stop'
  docker stop $argv;
end

function dcs --wraps='docker-compose stop' --description 'docker-compose stop'
  docker-compose stop $argv;
end

function drm --wraps='docker rm' --description 'docker rm'
  docker rm $argv;
end

function drmi --wraps='docker rmi' --description 'docker rmi'
  docker rmi $argv;
end

function dnls --wraps='docker dnls' --description 'docker network ls'
  docker network ls $argv;
end

function dnrm --wraps='docker dnrm' --description 'docker network rm'
  docker network rm $argv;
end

function din --wraps='docker din' --description 'docker inspect'
  docker inspect $argv;
end